package com.jhs.demo.rxandroid.pop;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.jhs.demo.MyApp;
import com.jhs.demo.R;
import com.jhs.demo.rxandroid.bean.InfoBean;
import com.jhs.demo.rxandroid.view.MyPopupWindow;
import com.jhs.demo.utils.MyUtils;


/**
 * Created by wangqi on 2018/8/30.
 */

public class InfoDetailPopupWindow {

    private InfoBean.Info info;
    private Activity activity;
    private MyPopupWindow mPopupWindow;
    private int animationStyle;
    private boolean isOutTouchable;
    private float bgAlpha = 0.4f;

    private ImageView iv_head_bg;
    private ImageView iv_head;
    private TextView tv_name;
    private TextView tv_cid;
    private TextView tv_sex;

    private PopupWindow init() {
        if (mPopupWindow == null) {
            View contentView = LayoutInflater.from(activity).inflate(R.layout.pop_info_detail, null);
            tv_name = contentView.findViewById(R.id.tv_name);
            tv_cid = contentView.findViewById(R.id.tv_cid);
            tv_sex = contentView.findViewById(R.id.tv_sex);
            iv_head = contentView.findViewById(R.id.iv_head);
            iv_head_bg = contentView.findViewById(R.id.iv_head_bg);

            mPopupWindow = new MyPopupWindow(contentView, ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, true);
        }
        //设置数据
        if (info != null) {
            tv_name.setText(info.getName());
            tv_cid.setText(info.getCid());
            tv_sex.setText(MyUtils.setSex(info.getSex()));
            MyUtils.setVagueImageFormat(MyApp.BASE_URL + info.getHeadPic(), iv_head_bg);
            MyUtils.setCircleImageFormat(MyApp.BASE_URL + info.getHeadPic(), iv_head);
        }

        mPopupWindow.setBgAlpha(bgAlpha);

        mPopupWindow.setAnimationStyle(animationStyle);
        // 设置PopupWindow的背景
        mPopupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // 设置PopupWindow是否能响应外部点击事件
        mPopupWindow.setOutsideTouchable(isOutTouchable);
        // 设置PopupWindow是否能响应点击事件
        mPopupWindow.setTouchable(true);
        mPopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        MyUtils.setBackgroundAlpha(activity, 1f);
                    }
                }, 200);
            }
        });
        return mPopupWindow;
    }

    public static class Builder {

        private final InfoDetailPopupWindow popupWindow;

        public Builder(Activity activity) {
            popupWindow = new InfoDetailPopupWindow();
            popupWindow.activity = activity;
        }

        public Builder setInfo(InfoBean.Info info) {
            popupWindow.info = info;
            return this;
        }
        public Builder setAnimationStyle(int animationStyle) {
            popupWindow.animationStyle = animationStyle;
            return this;
        }

        public Builder setOutTouchable(boolean outTouchable) {
            popupWindow.isOutTouchable = outTouchable;
            return this;
        }

        public Builder setBgAlpha(float bgAlpha){
            popupWindow.bgAlpha = bgAlpha;
            return this;
        }
        public PopupWindow creat(){
            return popupWindow.init();
        }
    }
}
