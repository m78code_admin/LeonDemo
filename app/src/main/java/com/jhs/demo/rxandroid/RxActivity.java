package com.jhs.demo.rxandroid;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.PopupWindow;


import com.jhs.demo.R;
import com.jhs.demo.rxandroid.adapter.RxAdapter;
import com.jhs.demo.rxandroid.bean.InfoBean;
import com.jhs.demo.rxandroid.bean.InfoList;
import com.jhs.demo.rxandroid.http.HttpLoader;
import com.jhs.demo.rxandroid.http.HttpObserver;
import com.jhs.demo.rxandroid.pop.DoubleChoosePopupWindow;
import com.jhs.demo.rxandroid.pop.InfoDetailPopupWindow;
import com.jhs.demo.rxandroid.view.SearchEdittext;

public class RxActivity extends AppCompatActivity implements SearchEdittext.OnEditTextChange,
        View.OnClickListener, RxAdapter.OnItemClickListener {

    private HttpLoader mLoader;
    private RxAdapter adapter;
    private PopupWindow popupWindow;
    private RecyclerView rectckerView;
    private SearchEdittext search_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rx);

        mLoader = new HttpLoader();

        initView();
        initData();
    }

    private void initView() {
        popupWindow = new DoubleChoosePopupWindow.Builder(this)
                .setContent("确认退出？")
                .setLeftText("取消")
                .setRightText("确认")
                .setOutTouchable(true)
                .setBgAlpha(0.7f)
                .setAnimationStyle(R.style.pop_animation)
                .setListener(this)
                .creat();

        search_view = findViewById(R.id.search_view);
        rectckerView = findViewById(R.id.recyclerView);

        rectckerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new RxAdapter(this);
        adapter.setOnItemClickListener(this);
        rectckerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        rectckerView.setAdapter(adapter);

        search_view.setOnEditTextChange(this);
    }

    private void initData() {
        mLoader.getInfoList(observerList);
    }

    @Override
    public void onValueChange(String s) {
        if (!TextUtils.isEmpty(s)) {
            int i;
            try {
                i = Integer.parseInt(s);
                mLoader.getInfo(i, observer);
            } catch (NumberFormatException e) {
                e.printStackTrace();
                initData();
            }
        } else {
            initData();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_left:
                popupWindow.dismiss();
                break;
            case R.id.tv_right:
                popupWindow.dismiss();
                finish();
                break;
        }
    }

    @Override
    public void itemClick(View view, int position) {
        if (adapter.getData() != null) {
            new InfoDetailPopupWindow.Builder(this)
                    .setInfo(adapter.getData().get(position))
                    .setAnimationStyle(R.style.pop_animation)
                    .setBgAlpha(0.7f)
                    .creat()
                    .showAtLocation(rectckerView, Gravity.CENTER, 0, 0);
        }
    }

    @Override
    public void onBackPressed() {
        popupWindow.showAtLocation(rectckerView, Gravity.CENTER, 0, 0);
    }

    HttpObserver<InfoBean> observer = new HttpObserver<InfoBean>() {
        @Override
        protected void onHttpError(Throwable e) {
        }

        @Override
        protected void onHttpSuccess(InfoBean bean) {
            if (bean.getCode() == 200) {
                adapter.setItem(bean.getData());
            }
        }
    };

    HttpObserver<InfoList> observerList = new HttpObserver<InfoList>() {
        @Override
        protected void onHttpError(Throwable e) {
        }

        @Override
        protected void onHttpSuccess(InfoList bean) {
            if (bean.getCode() == 200) {
                adapter.setData(bean.getData());
            }
        }
    };

    public static void OpenActivity(Context context) {
        context.startActivity(new Intent(context, RxActivity.class));
    }
}
