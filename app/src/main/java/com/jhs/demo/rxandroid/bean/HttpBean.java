package com.jhs.demo.rxandroid.bean;

/**
 * http请求 数据基类
 * Created by wangqi on 2018/8/24.
 */

public class HttpBean {
    private int code;
    private String message;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

}
