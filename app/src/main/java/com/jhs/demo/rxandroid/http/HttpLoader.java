package com.jhs.demo.rxandroid.http;

import com.jhs.demo.rxandroid.bean.InfoBean;
import com.jhs.demo.rxandroid.bean.InfoList;

/**
 * 将网络请求抽取到Loader中 避免代码臃肿
 * Created by wangqi on 2018/8/24.
 */

public class HttpLoader extends ObjectLoader {

    private HttpService mHttpService;

    public HttpLoader() {
        mHttpService = ServiceManager.getInstance().creat(HttpService.class);
    }

    /**
     * 通过id查询
     * @param cid 参数
     * @param observer 回调
     */
    public void getInfo(int cid, HttpObserver<InfoBean> observer) {
        observable(mHttpService.getInfo(cid)).subscribe(observer);
    }

    public void getInfoList(HttpObserver<InfoList> observer){
        observable(mHttpService.getInfoList()).subscribe(observer);
    }
}
