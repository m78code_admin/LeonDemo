package com.jhs.demo.rxandroid.http;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by wangqi on 2018/8/24.
 */

public class HttpCommonInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        //TODO 拦截器

        return chain.proceed(request);
    }
}
