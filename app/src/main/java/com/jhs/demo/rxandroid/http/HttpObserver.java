package com.jhs.demo.rxandroid.http;


import com.jhs.demo.MyApp;
import com.jhs.demo.rxandroid.bean.HttpBean;
import com.jhs.demo.utils.MyUtils;


import rx.Observer;

/**
 * Created by wangqi on 2018/8/24.
 */

public abstract class HttpObserver<T> implements Observer<T> {

    protected abstract void onHttpError(Throwable e);

    protected abstract void onHttpSuccess(T bean);

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {
        onHttpError(e);
        MyUtils.showToast(MyApp.getInstance(), e.getMessage());
    }

    @Override
    public void onNext(T t) {
        if (t instanceof HttpBean) {
            if (((HttpBean) t).getCode() == 200)
                onHttpSuccess(t);
            else
                MyUtils.showToast(MyApp.getInstance(), ((HttpBean) t).getMessage());
        } else {
            onHttpError(new Exception("数据转换发生异常"));
        }
    }
}
