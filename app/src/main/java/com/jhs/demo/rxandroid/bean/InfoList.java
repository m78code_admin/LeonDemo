package com.jhs.demo.rxandroid.bean;

import java.util.List;

/**
 * Created by wangqi on 2018/8/28.
 */

public class InfoList extends HttpBean {
    private List<InfoBean.Info> data;

    public List<InfoBean.Info> getData() {
        return data;
    }
}
