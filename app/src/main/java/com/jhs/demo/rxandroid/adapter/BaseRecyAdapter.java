package com.jhs.demo.rxandroid.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wangqi on 2018/8/30.
 */

public abstract class BaseRecyAdapter<T> extends RecyclerView.Adapter<BaseRecyAdapter.BaseViewHolder> {

    protected abstract void onBaseBindView(BaseViewHolder baseViewHolder, int i);

    protected abstract BaseViewHolder onCreateBaseViewHolder(ViewGroup viewGroup, int i);

    protected OnItemClickListener listener;
    protected Context mContext;
    protected List<T> mList;

    public BaseRecyAdapter(Context context) {
        this.mContext = context;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public List<T> getData() {
        return mList;
    }

    public void setData(List<T> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }

    public void setItem(T t) {
        if (this.mList == null)
            this.mList = new ArrayList<>();
        mList.clear();
        mList.add(t);
        notifyDataSetChanged();
    }

    public void addData(List<T> mList) {
        if (this.mList == null)
            this.mList = new ArrayList<>();
        this.mList.addAll(mList);
        notifyDataSetChanged();
    }

    public void addItem(T t) {
        if (this.mList == null)
            this.mList = new ArrayList<>();
        this.mList.add(t);
        notifyDataSetChanged();
    }

    public T getItem(int i) {
        return mList != null ? mList.get(i) : null;
    }
    //TODO 需要啥自己加

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return onCreateBaseViewHolder(viewGroup,i);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder baseViewHolder, int i) {
        onBaseBindView(baseViewHolder, i);
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public interface OnItemClickListener {
        void itemClick(View v, int position);
    }

    public static class BaseViewHolder extends RecyclerView.ViewHolder {
        public BaseViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }

}
