package com.jhs.demo.rxandroid.pop;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.jhs.demo.R;
import com.jhs.demo.rxandroid.view.MyPopupWindow;
import com.jhs.demo.utils.MyUtils;


/**
 * 选择pop  取消/确认
 * Created by wangqi on 2018/8/29.
 */

public class DoubleChoosePopupWindow {

    private MyPopupWindow mPopupWindow;
    private TextView tv_content, tv_left, tv_right;
    private Activity activity;

    //属性
    private String content;
    private String leftText;

    private String rightText;

    private int animationStyle;

    private boolean isOutTouchable;

    private View.OnClickListener listener;

    private float bgAlpha =0.4f;

    private PopupWindow init() {
        if (mPopupWindow == null) {
            View contentView = LayoutInflater.from(activity).inflate(R.layout.pop_double_choose, null);
            tv_content = contentView.findViewById(R.id.tv_content);
            tv_left = contentView.findViewById(R.id.tv_left);
            tv_right = contentView.findViewById(R.id.tv_right);


            mPopupWindow = new MyPopupWindow(contentView, ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, true);
        }
        mPopupWindow.setBgAlpha(bgAlpha);

        mPopupWindow.setAnimationStyle(animationStyle);
        // 设置PopupWindow的背景
        mPopupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // 设置PopupWindow是否能响应外部点击事件
        mPopupWindow.setOutsideTouchable(isOutTouchable);
        // 设置PopupWindow是否能响应点击事件
        mPopupWindow.setTouchable(true);
        mPopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        MyUtils.setBackgroundAlpha(activity, 1f);
                    }
                },200);
            }
        });

        tv_content.setText(content);
        tv_left.setText(leftText);
        tv_right.setText(rightText);

        if (listener!=null){
            tv_left.setOnClickListener(listener);
            tv_right.setOnClickListener(listener);
        }

        return mPopupWindow;
    }

    public static class Builder {
        private DoubleChoosePopupWindow popupWindow;

        public Builder(Activity activity) {
            popupWindow = new DoubleChoosePopupWindow();
            popupWindow.activity =activity;
        }

        public Builder setContent(String content) {
            popupWindow.content = content;
            return this;
        }

        public Builder setLeftText(String leftText) {
            popupWindow.leftText = leftText;
            return this;
        }

        public Builder setRightText(String rightText) {
            popupWindow.rightText = rightText;
            return this;
        }

        public Builder setAnimationStyle(int animationStyle) {
            popupWindow.animationStyle = animationStyle;
            return this;
        }

        public Builder setOutTouchable(boolean outTouchable) {
            popupWindow.isOutTouchable = outTouchable;
            return this;
        }

        public Builder setListener(View.OnClickListener listener){
            popupWindow.listener = listener;
            return this;
        }
        public Builder setBgAlpha(float bgAlpha){
            popupWindow.bgAlpha = bgAlpha;
            return this;
        }
        public PopupWindow creat(){
            return popupWindow.init();
        }
    }
}
