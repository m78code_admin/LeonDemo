package com.jhs.demo.rxandroid.bean;

/**
 * Created by wangqi on 2018/8/24.
 */

public class InfoBean extends HttpBean {

    /**
     * cid : 8
     * name : 小明
     * sex : woman
     * creatTime : 2018-08-23 10:19:36
     * updateTime : 2018-08-23 10:21:46
     */

    private Info data;

    public Info getData() {
        return data;
    }

    public static class Info {
        private String cid;
        private String name;
        private String sex;
        private String creatTime;
        private String updateTime;
        private String headPic;

        public String getHeadPic() {
            return headPic;
        }

        public void setHeadPic(String headPic) {
            this.headPic = headPic;
        }

        public String getCid() {
            return cid;
        }

        public void setCid(String cid) {
            this.cid = cid;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getCreatTime() {
            return creatTime;
        }

        public void setCreatTime(String creatTime) {
            this.creatTime = creatTime;
        }

        public String getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(String updateTime) {
            this.updateTime = updateTime;
        }
    }
}
