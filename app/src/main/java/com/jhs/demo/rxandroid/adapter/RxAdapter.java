package com.jhs.demo.rxandroid.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jhs.demo.MyApp;
import com.jhs.demo.R;
import com.jhs.demo.rxandroid.bean.InfoBean;
import com.jhs.demo.utils.MyUtils;


/**
 * Created by wangqi on 2018/8/28.
 */

public class RxAdapter extends BaseRecyAdapter<InfoBean.Info> {

    public RxAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    protected void onBaseBindView(BaseViewHolder baseViewHolder, final int i) {
        RxViewHolder viewHolder = (RxViewHolder) baseViewHolder;
        InfoBean.Info bean = mList.get(i);
        if (bean != null) {
            viewHolder.tvName.setText(bean.getName());
            MyUtils.setCircleImageFormat(MyApp.BASE_URL + bean.getHeadPic(), viewHolder.ivPic);
        }
        viewHolder.ll_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.itemClick(v, i);
                }
            }
        });
    }

    @Override
    protected BaseViewHolder onCreateBaseViewHolder(ViewGroup viewGroup, int i) {
        return new RxViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_rx_view, viewGroup, false));
    }

    public static class RxViewHolder extends BaseViewHolder {
        ImageView ivPic;
        TextView tvName;
        View ll_edit,ll_content;

        public RxViewHolder(@NonNull View itemView) {
            super(itemView);
            ivPic = itemView.findViewById(R.id.iv_pic);
            tvName = itemView.findViewById(R.id.tv_name);
            ll_content = itemView.findViewById(R.id.ll_content);
            ll_edit = itemView.findViewById(R.id.ll_edit);
        }
    }
}
