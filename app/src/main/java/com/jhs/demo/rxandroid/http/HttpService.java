package com.jhs.demo.rxandroid.http;


import com.jhs.demo.rxandroid.bean.InfoBean;
import com.jhs.demo.rxandroid.bean.InfoList;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by wangqi on 2018/8/24.
 */

public interface HttpService {

    @GET("demo1/day2/getInfo.php")
    Observable<InfoBean> getInfo(@Query("cid") int cid);

    @GET("demo1/day2/getInfo.php")
    Observable<InfoList> getInfoList();


}
