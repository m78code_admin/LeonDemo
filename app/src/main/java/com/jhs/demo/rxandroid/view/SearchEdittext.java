package com.jhs.demo.rxandroid.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.jhs.demo.R;


/**
 * Created by wangqi on 2018/8/27.
 */

public class SearchEdittext extends LinearLayout implements TextWatcher, View.OnClickListener {

    public interface OnEditTextChange{
        void onValueChange(String s);
    }
    private OnEditTextChange onEditTextChange;
    private EditText et_search_input;

    public SearchEdittext(Context context) {
        this(context, null);
    }

    public SearchEdittext(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SearchEdittext(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setOrientation(LinearLayout.VERTICAL);
        setFocusable(true);
        setFocusableInTouchMode(true);
        View searchView = LayoutInflater.from(getContext()).inflate(R.layout.view_search_edittet, this, false);
        et_search_input = searchView.findViewById(R.id.et_search_input);
        ImageView iv_delete = searchView.findViewById(R.id.iv_delete);
        et_search_input.addTextChangedListener(this);
        iv_delete.setOnClickListener(this);


        addView(searchView);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_delete:
                et_search_input.setText("");
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (onEditTextChange!=null){
            onEditTextChange.onValueChange(s.toString());
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    public void setOnEditTextChange(OnEditTextChange onEditTextChange) {
        this.onEditTextChange = onEditTextChange;
    }
}
