package com.jhs.demo.rxandroid.view;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.PopupWindow;

import com.jhs.demo.utils.MyUtils;


/**
 * 抽取了 设置背景功能
 * Created by wangqi on 2018/8/29.
 */

public class MyPopupWindow extends PopupWindow {

    private float bgAlpha;

    public MyPopupWindow(Context context) {
        super(context);
    }

    public MyPopupWindow(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyPopupWindow(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public MyPopupWindow(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public MyPopupWindow() {
    }

    public MyPopupWindow(View contentView) {
        super(contentView);
    }

    public MyPopupWindow(int width, int height) {
        super(width, height);
    }

    public MyPopupWindow(View contentView, int width, int height) {
        super(contentView, width, height);
    }

    public MyPopupWindow(View contentView, int width, int height, boolean focusable) {
        super(contentView, width, height, focusable);
    }

    @Override
    public void showAtLocation(View parent, int gravity, int x, int y) {
        MyUtils.setBackgroundAlpha((Activity) parent.getContext(),bgAlpha);
        super.showAtLocation(parent, gravity, x, y);
    }

    @Override
    public void showAsDropDown(View anchor) {
        MyUtils.setBackgroundAlpha((Activity) anchor.getContext(),bgAlpha);
        super.showAsDropDown(anchor);
    }

    @Override
    public void showAsDropDown(View anchor, int xoff, int yoff) {
        MyUtils.setBackgroundAlpha((Activity) anchor.getContext(),bgAlpha);
        super.showAsDropDown(anchor, xoff, yoff);
    }

    @Override
    public void showAsDropDown(View anchor, int xoff, int yoff, int gravity) {
        MyUtils.setBackgroundAlpha((Activity) anchor.getContext(),bgAlpha);
        super.showAsDropDown(anchor, xoff, yoff, gravity);
    }

    public void setBgAlpha(float bgAlpha) {
        this.bgAlpha = bgAlpha;
    }
}
