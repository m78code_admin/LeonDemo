package com.jhs.demo.dragimgs;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.jhs.demo.R;

import java.util.ArrayList;
import java.util.List;

public class DragImagesActivity extends AppCompatActivity {

    private DragImageView drag_imag_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drag_images);

        drag_imag_view = findViewById(R.id.drag_imag_view);
        List<Integer> imgs = new ArrayList<>(5);
        imgs.add(R.mipmap.img1);
        imgs.add(R.mipmap.img2);
        imgs.add(R.mipmap.img3);
        imgs.add(R.mipmap.img4);
        imgs.add(R.mipmap.img5);
        drag_imag_view.setData(imgs);
    }

    public static void OpenActivity(Context context){
        Intent intent = new Intent(context,DragImagesActivity.class);
        context.startActivity(intent);
    }
}
