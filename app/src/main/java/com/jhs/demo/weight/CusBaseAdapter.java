package com.jhs.demo.weight;

import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;


/**
 * Created by ChengYan Zhang
 * on 2019/5/23
 */
public abstract class CusBaseAdapter<T> extends android.widget.BaseAdapter {

    private List<T> datas;

    protected OnClickListener<T> onItemClickListener;
    protected OnLongClickListener<T> onItemLongClickListener;

    public void setOnItemLongClickListener(OnLongClickListener<T> onItemLongClickListener) {
        this.onItemLongClickListener = onItemLongClickListener;
    }

    public void setOnItemClickListener(OnClickListener<T> onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setDatas(List<T> datas) {
        this.datas = datas;
        notifyDataSetChanged();
    }

    public void addDatas(List<T> datas) {
        if (null == this.datas) setDatas(datas);
        else {
            this.datas.addAll(datas);
            notifyDataSetChanged();
        }
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getCount() {
        return null == datas ? 0 : datas.size();
    }

    @Override
    public T getItem(int position) {
        return datas.get(position);
    }

    protected @LayoutRes
    abstract int layoutId();

    protected View createView(ViewGroup viewGroup) {
        return LayoutInflater.from(viewGroup.getContext()).inflate(layoutId(), viewGroup, false);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null == convertView ? new ViewHolder(convertView = createView(parent)) : getHolder(convertView);
        holder.position = position;
        bindData(holder, getItem(position));
        return convertView;
    }

    protected ViewHolder getHolder(View view) {
        return (ViewHolder) view.getTag();
    }

    public void initViewHolder(ViewHolder viewHolder) {
        viewHolder.view.setOnClickListener(viewHolder);
        viewHolder.view.setOnLongClickListener(viewHolder);
    }

    public class ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private int position;
        private View view;
        private SparseArray<View> viewSparseArray;

        public View getRootView() {
            return view;
        }

        public void setText(@IdRes int id, CharSequence text) {
            ((TextView) getView(id)).setText(text);
        }

        public ViewHolder(View view) {
            this.view = view;
            view.setTag(this);
            viewSparseArray = new SparseArray<>();
            initViewHolder(this);
        }

        public <E extends View> E getView(@IdRes int id) {
            View view = viewSparseArray.get(id, null);
            if (null == view) {
                viewSparseArray.put(id, view = this.view.findViewById(id));
            }
            return (E) view;
        }

        @Override
        public void onClick(View v) {
            if (null != onItemClickListener) onItemClickListener.onClick(getItem(position));
        }

        @Override
        public boolean onLongClick(View v) {
            if (null != onItemLongClickListener) {
                onItemLongClickListener.onLongClick(getItem(position));
                return true;
            }
            return false;
        }

    }

    public abstract void bindData(ViewHolder holder, T t);

    public interface OnClickListener<T> {
        void onClick(T t);
    }

    public interface OnLongClickListener<T> {
        void onLongClick(T t);
    }

}
