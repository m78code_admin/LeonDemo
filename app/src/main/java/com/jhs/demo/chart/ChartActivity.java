package com.jhs.demo.chart;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.jhs.demo.R;
import com.jhs.demo.chart.fragment.KChartFragment;
import com.jhs.demo.chart.fragment.TSChartFragment;

import java.util.ArrayList;
import java.util.List;

public class ChartActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;

    private static String[] titles = {"分时","日K","5分","15分","30分","60分"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_line);

        tabLayout = findViewById(R.id.tab_layout);
        viewPager = findViewById(R.id.view_pager);

        List<Fragment> fragments = new ArrayList<>();
        fragments.add(new TSChartFragment());
        fragments.add(new KChartFragment());
        fragments.add(new TSChartFragment());
        fragments.add(new TSChartFragment());
        fragments.add(new TSChartFragment());
        fragments.add(new TSChartFragment());
        viewPager.setAdapter(new MyPagerAdapter(getSupportFragmentManager(),fragments,titles));
        viewPager.setOffscreenPageLimit(fragments.size());
        tabLayout.setupWithViewPager(viewPager);
    }

    public static void OpenActivity(Context context){
        Intent intent = new Intent(context,ChartActivity.class);
        context.startActivity(intent);
    }
}
