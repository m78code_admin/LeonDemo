package com.jhs.demo;

import android.Manifest;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.blankj.utilcode.util.ToastUtils;
import com.jhs.demo.asynctask.AsyncTaskActivity;
import com.jhs.demo.coreprogress.CoreProgressActivity;
import com.jhs.demo.dragimgs.DragImagesActivity;
import com.jhs.demo.dragrecy.DragRecyActivity;
import com.jhs.demo.groupmember.GroupMemberActivity;
import com.jhs.demo.handpsd.HandPsdActivity;
import com.jhs.demo.loading.LoadingActivity;
import com.jhs.demo.progress.ProcessIVActivity;
import com.jhs.demo.rxandroid.RxActivity;
import com.jhs.demo.chart.ChartActivity;
import com.jhs.demo.upload.UploadImageActivity;
import com.jhs.demo.weight.CusBaseAdapter;
import com.yanzhenjie.permission.Action;
import com.yanzhenjie.permission.AndPermission;
import com.yzq.zxinglibrary.android.CaptureActivity;
import com.yzq.zxinglibrary.common.Constant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private List<String> datas = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addDatas();
        ListView mListView = findViewById(R.id.list_view);
        //        FunAdapter adapter = new FunAdapter();
        MyAdapter adapter = new MyAdapter();
        mListView.setAdapter(adapter);
        //        adapter.setDatas(datas);
        mListView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case 0:
                DragRecyActivity.OpenActivity(MainActivity.this);
                break;
            case 1:
                DragImagesActivity.OpenActivity(MainActivity.this);
                break;
            case 2:
                HandPsdActivity.OpenActivity(this);
                break;
            case 3:
                GroupMemberActivity.OpenActivity(this);
                break;
            case 4:
                LoadingActivity.OpenActivity(this);
                break;
            case 5:
                RxActivity.OpenActivity(this);
                break;
            case 6:
                ChartActivity.OpenActivity(this);
                break;
            case 7:
                requestPermission();
                break;
            case 8:
                ToastUtils.showLong("指纹识别");
                break;
            case 9:
                //                ToastUtils.showLong("权限申请");
                callPhonePermission();
                break;
            case 10:
                //                ToastUtils.showLong("多线程");
                AsyncTaskActivity.OpenActivity(this);
                break;
            case 11:
                ProcessIVActivity.OpenActivity(this);
                break;
            case 12:
                CoreProgressActivity.OpenActivity(this);
                break;
            case 13:
                UploadImageActivity.OpenActivity(this);
                break;
        }
    }

    private void addDatas() {
        String[] titles = getResources().getStringArray(R.array.data_name);
        Collections.addAll(datas, titles);
    }

    private void requestPermission() {
        AndPermission.with(this)
                .runtime()
                .permission(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .onGranted(new Action<List<String>>() {
                    @Override
                    public void onAction(List<String> data) {
                        sm();
                    }
                })
                .onDenied(new Action<List<String>>() {
                    @Override
                    public void onAction(List<String> data) {
                        for (String permission : data) {
                            if (AndPermission.hasAlwaysDeniedPermission(MainActivity.this, permission)) {
                                ToastUtils.showLong("你拒绝了 " + (Manifest.permission.CAMERA.equals(permission) ? "相机" : "存储" + " 相关权限，如要继续使用该功能，请前往设置中心手动为本应用打开该权限"));
                                return;
                            }
                        }
                        ToastUtils.showLong("您需要授予相关权限");
                    }
                }).start();
    }

    public static final int REQUEST_CODE_SCAN = 11111;

    private void sm() {
        Intent intent = new Intent(MainActivity.this, CaptureActivity.class);
        //ZxingConfig config = new ZxingConfig();
        //config.setShowbottomLayout(true);//底部布局（包括闪光灯和相册）
        //config.setPlayBeep(true);//是否播放提示音
        //config.setShake(true);//是否震动
        //config.setShowAlbum(true);//是否显示相册
        //config.setShowFlashLight(true);//是否显示闪光灯
        //intent.putExtra(Constant.INTENT_ZXING_CONFIG, config);
        startActivityForResult(intent, REQUEST_CODE_SCAN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_SCAN && resultCode == RESULT_OK) {
            if (null != data) {
                String content = data.getStringExtra(Constant.CODED_CONTENT);
                Log.i("xxx", "onActivityResult: 扫描结果为 " + content);
            }
        }
    }

    /****************权限申请******************/
    private void callPhonePermission() {
         /*
        PermissionsDispatcher是我们实现了@RuntimePermissions和@NeedsPermission这两个必须的注解之后，再build一次project之后，
        编译器就会在在app\build\intermediates\classes\debug目录下与被注解的Activity同一个包。
         */
        //        MainActivityPermissionsDispatcher.callWithCheck(this);
    }

    private class MyAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return datas.size();
        }

        @Override
        public String getItem(int position) {
            return datas.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (null == convertView) {
                holder = new ViewHolder(convertView = getLayoutInflater().inflate(R.layout.adapter_fun_item, parent, false));
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.itemName.setText(getItem(position));
            return convertView;
        }
    }

    class ViewHolder {

        TextView itemName;

        public ViewHolder(View convertView) {
            itemName = convertView.findViewById(R.id.fun_item_name);
        }
    }
}
