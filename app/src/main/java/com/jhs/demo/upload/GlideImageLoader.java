package com.jhs.demo.upload;

import android.app.Activity;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.jhs.demo.R;
import com.lzy.imagepicker.loader.ImageLoader;

public class GlideImageLoader implements ImageLoader {

    @Override
    public void displayImage(Activity activity, String path, ImageView imageView, int width, int height) {
        RequestOptions options = new RequestOptions().centerCrop()
                .placeholder(R.mipmap.ic_launcher_round)//占位即加载中的图片。
                .error(R.mipmap.ic_launcher_round)//错误图片.
                .fallback(R.mipmap.ic_launcher_round);// 当url为null的时候，判断是否设置了fallback，是的话则显示fallback图片，否的话显示error图片，如果error还是没有设置则显示placeholder图片.

        Glide.with(activity)
                .load(path)
                .apply(options)
                .into(imageView);
    }

    @Override
    public void displayImagePreview(Activity activity, String path, ImageView imageView, int width, int height) {

    }

    @Override
    public void clearMemoryCache() {

    }
}
