package com.jhs.demo.dragrecy;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.jhs.demo.R;

import java.util.ArrayList;
import java.util.List;

public class DragRecyActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private SimpleRecycleAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drag_recy);
        recyclerView = findViewById(R.id.recyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new SimpleRecycleAdapter(this);

        ItemTouchHelper.Callback simpleCallback = new SimpleItemTouchHelperCallback(adapter);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);

        recyclerView.setAdapter(adapter);

        List<String> list = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            list.add("测试数据"+i);
        }
        adapter.setData(list);
    }

    public static void OpenActivity(Context context){
        Intent intent = new Intent(context,DragRecyActivity.class);
        context.startActivity(intent);
    }
}
