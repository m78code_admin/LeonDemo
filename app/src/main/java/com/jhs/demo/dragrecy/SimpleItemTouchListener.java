package com.jhs.demo.dragrecy;

/**
 * Created by wangqi on 2018/7/31.
 */
public interface SimpleItemTouchListener {
    void onItemDrag(int fromPosition, int toPosition);
    void onItemDelete(int position);

}
