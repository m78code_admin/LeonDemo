package com.jhs.demo.progress;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.blankj.utilcode.util.ToastUtils;
import com.jhs.demo.R;

public class ProcessIVActivity extends AppCompatActivity implements View.OnClickListener {

    ProcessImageView processImageView;
    Button btnUpload;
    private final int SUCCESS = 0;
    int progress = 0;

    @SuppressLint("HandlerLeak")
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case SUCCESS:
                    ToastUtils.showLong("图片上传完成");
                    processImageView.setVisibility(View.GONE);
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_process_iv);

        processImageView = (ProcessImageView) findViewById(R.id.image);
        btnUpload = (Button) findViewById(R.id.btn_upload);

        btnUpload.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        //模拟图片上传进度
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    if (progress == 100) {//图片上传完成
                        handler.sendEmptyMessage(SUCCESS);
                        return;
                    }
                    progress++;
                    processImageView.setProgress(progress);
                    try {
                        Thread.sleep(200);  //暂停0.2秒
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    public static void OpenActivity(Context context) {
        context.startActivity(new Intent(context, ProcessIVActivity.class));
    }
}
