package com.jhs.demo.groupmember;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wangqi on 2018/8/7.
 */
public class GroupMemberView extends RelativeLayout {

    private List<Integer> mList = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private GroupMemberAdapter mAdapter;

    public GroupMemberView(@NonNull Context context) {
        this(context, null);
    }

    public GroupMemberView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GroupMemberView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mRecyclerView = new RecyclerView(context);
        mAdapter = new GroupMemberAdapter(context);
        mRecyclerView.setAdapter(mAdapter);
        RelativeLayout.LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        addView(mRecyclerView, params);
    }

    /**
     * 初始化View
     */
    @SuppressLint("NewApi")
    private void refreshView() {
        try {
            //正方形边长
            int width = getWidth() < getHeight() ? getWidth() : getHeight();
            int border = 0;
            LayoutParams params = (LayoutParams) mRecyclerView.getLayoutParams();
            //确定每个item的宽高
            //且item为正方形
            if (mList.size() == 0)
                return;
            if (mList.size() == 1) {
                //TODO 一个人群聊个毛啊
            } else if (mList.size() > 1 && mList.size() < 5) {
                border = width / 2;
                if (mList.size()==2){
                    //需要recycleView居中
                    params.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
                }else{
                    params.removeRule(RelativeLayout.CENTER_IN_PARENT);
                }
                mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
            } else {
                border = width / 3;
                params.removeRule(RelativeLayout.CENTER_IN_PARENT);
                mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
            }
            mAdapter.setData(mList, border);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setData(List<Integer> list) {
        if (list!=null&&list.size()!=0){
            mList.clear();
            if (list.size()>9){
                list = list.subList(0, 9);
            }
            this.mList.addAll(list);
        }
        post(new Runnable() {
            @Override
            public void run() {
                refreshView();
            }
        });
    }

}
