package com.jhs.demo.groupmember;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import com.jhs.demo.R;


public class GroupMemberActivity extends AppCompatActivity implements View.OnClickListener {

    private ConferenceMemberViewGroup group_member_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_member);

        findViewById(R.id.bt_1).setOnClickListener(this);
        findViewById(R.id.bt_2).setOnClickListener(this);
        findViewById(R.id.bt_3).setOnClickListener(this);
        findViewById(R.id.bt_4).setOnClickListener(this);
        findViewById(R.id.bt_5).setOnClickListener(this);
        findViewById(R.id.bt_6).setOnClickListener(this);
        findViewById(R.id.bt_7).setOnClickListener(this);
        findViewById(R.id.bt_8).setOnClickListener(this);
        findViewById(R.id.bt_9).setOnClickListener(this);
        group_member_view = findViewById(R.id.group_member_view);

    }

    public static void OpenActivity(Context context) {
        context.startActivity(new Intent(context, GroupMemberActivity.class));
    }

    @Override
    public void onClick(View view) {
        group_member_view.removeAllViews();
        int count = Integer.parseInt(((Button) view).getText().toString());
        for (int i = 0; i < count; i++) {
            ImageView imageView = new ImageView(this);
//            ViewGroup.LayoutParams params = new ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//            imageView.setLayoutParams(params);
            imageView.setImageResource(R.mipmap.img2);
//            imageView.setLayoutParams(params);
            group_member_view.addView(imageView);
        }

    }
}
