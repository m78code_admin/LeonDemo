package com.jhs.demo.utils;

import android.util.Log;

/**
 * Created by wangqi on 2018/8/29.
 */

public class LogUtils {
    private static final String TAG = "wangqi";

    public static void e(String msg){
        Log.e(TAG,msg);
    }
}
