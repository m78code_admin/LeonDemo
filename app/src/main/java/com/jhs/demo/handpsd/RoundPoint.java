package com.jhs.demo.handpsd;

/**
 * Created by wangqi on 2018/8/2.
 */
public class RoundPoint {
    public float pointX;
    public float pointY;

    public RoundPoint(float pointX, float pointY) {
        this.pointX = pointX;
        this.pointY = pointY;
    }

}
