package com.jhs.demo.handpsd;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.jhs.demo.R;


public class HandPsdActivity extends AppCompatActivity {

    private HandPsdView hand_password_view;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hand_psd);
        hand_password_view = findViewById(R.id.hand_password_view);
        hand_password_view.setHandListener(new HandPsdView.HandViewListener() {
            @Override
            public void onComplete(String password) {
//                Toast.makeText(HandPsdActivity.this,password,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSettingSuccess() {
                finish();
            }
        });
    }

    public static void OpenActivity(Context context){
        Intent intent = new Intent(context,HandPsdActivity.class);
        context.startActivity(intent);
    }
}
