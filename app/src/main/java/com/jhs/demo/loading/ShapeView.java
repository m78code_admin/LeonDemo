package com.jhs.demo.loading;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;


import com.jhs.demo.utils.MyUtils;

import java.lang.ref.WeakReference;

/**
 * Created by wangqi on 2018/8/8.
 */
public class ShapeView extends View {
    private Paint mPaint;
    //当前的形状
    private Shape shape = Shape.SHAPE_TRIANGLE;
    //形状宽度
    private int mWidth = MyUtils.dp2px(25);
    private MyHandler handler;

    private int time;
    private int tempTime;

    enum Shape {
        SHAPE_RECT(Color.GREEN), SHAPE_TRIANGLE(Color.BLUE), SHAPE_CIRCULAR(Color.RED);

        public int color;

        Shape(int color) {
            this.color = color;
        }
    }

    public ShapeView(Context context) {
        this(context, null);
    }

    public ShapeView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ShapeView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int width = MeasureSpec.getSize(widthMeasureSpec);
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);

        if (widthMode == MeasureSpec.AT_MOST && heightMode == MeasureSpec.AT_MOST) {
            setMeasuredDimension(mWidth, mWidth);
        } else if (widthMode == MeasureSpec.AT_MOST) {
            setMeasuredDimension(mWidth, height);
        } else if (heightMode == MeasureSpec.AT_MOST) {
            setMeasuredDimension(width, mWidth);
        }
    }

    private void init() {
        setBackgroundColor(Color.TRANSPARENT);

        handler = new MyHandler(this);

        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.translate(getWidth() / 2, getHeight() / 2);

        mPaint.setColor(shape.color);
        Path path = new Path();
        switch (shape) {
            case SHAPE_RECT:
                path.reset();
                path.addRect(-mWidth / 2, -mWidth / 2, mWidth / 2, mWidth / 2, Path.Direction.CW);
                canvas.drawPath(path, mPaint);
                break;
            case SHAPE_TRIANGLE:
                path.reset();
                path.moveTo(0, -mWidth/2);
                path.lineTo(-mWidth / 2, mWidth / 2);
                path.lineTo(mWidth / 2, mWidth / 2);
                path.close();
                canvas.drawPath(path, mPaint);
                break;
            case SHAPE_CIRCULAR:
                path.reset();
                path.addCircle(0, 0, mWidth / 2, Path.Direction.CW);
                canvas.drawPath(path, mPaint);
                break;
        }
    }

    public void setTime(int time){
        this.time = time;
        tempTime=time;
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                handler.sendEmptyMessage(1);
            }
        },50);
    }

    private static class MyHandler extends Handler {
        WeakReference<ShapeView> view;

        public MyHandler(ShapeView view) {
            this.view = new WeakReference<>(view);
        }

        @Override
        public void handleMessage(Message msg) {
            final ShapeView shapeView = view.get();
            if (shapeView.time == 0) {
                shapeView.shape = shapeView.shape == Shape.SHAPE_RECT ?
                        Shape.SHAPE_TRIANGLE : shapeView.shape == Shape.SHAPE_TRIANGLE ?
                        Shape.SHAPE_CIRCULAR : Shape.SHAPE_RECT;
                shapeView.time=shapeView.tempTime;
                shapeView.postInvalidate();
            }
            shapeView.time--;
            shapeView.handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    shapeView.handler.sendEmptyMessage(1);
                }
            },1000);
        }
    }
}
