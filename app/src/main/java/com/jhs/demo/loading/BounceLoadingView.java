package com.jhs.demo.loading;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.RelativeLayout;
import com.jhs.demo.R;
import com.jhs.demo.utils.MyUtils;


/**
 * 弹跳 loading
 * Created by wangqi on 2018/8/8.
 */
public class BounceLoadingView extends RelativeLayout {

    private static final int ANIMATION_DURATION = 1000;
    private static final int ID_SHAPE_VIEW = 1;
    private static final int ID_IMAGE_VIEW = 2;

    private ShapeView shapeView;
    private AnimatorSet animatorSet;

    public BounceLoadingView(Context context) {
        this(context, null);
    }

    public BounceLoadingView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BounceLoadingView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        initView();
    }

    @SuppressLint("ResourceType")
    private void initView() {

        shapeView = new ShapeView(getContext());
        shapeView.setTime(ANIMATION_DURATION / 1000);
        shapeView.setId(ID_SHAPE_VIEW);
        LayoutParams shapeParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        shapeParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        addView(shapeView, shapeParams);
        //添加阴影
        CircleImageView imageView = new CircleImageView(getContext());
        imageView.setImageResource(getContext().getResources().getColor(R.color.colorPrimaryDark));
        LayoutParams imageParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        imageParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        imageParams.addRule(RelativeLayout.BELOW,ID_SHAPE_VIEW);
        imageParams.width = MyUtils.dp2px(10);
        imageParams.height = MyUtils.dp2px(10);
        addView(imageView,imageParams);

        animatorSet = new AnimatorSet();
        //下落动画
        ObjectAnimator transAnimator = ObjectAnimator.ofFloat(shapeView, "translationY", 0, -300, 0);
        transAnimator.setInterpolator(new MyInterpolator());
        transAnimator.setRepeatCount(ObjectAnimator.INFINITE);
        transAnimator.setRepeatMode(ObjectAnimator.RESTART);

        //旋转
        ObjectAnimator rotationAnimator = ObjectAnimator.ofFloat(shapeView, "rotation", 0, -180, -180);
        rotationAnimator.setInterpolator(new LinearInterpolator());
        rotationAnimator.setRepeatCount(ObjectAnimator.INFINITE);
        rotationAnimator.setRepeatMode(ObjectAnimator.RESTART);

        animatorSet.playTogether(transAnimator, rotationAnimator);
        animatorSet.setDuration(ANIMATION_DURATION);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        animatorSet.start();
    }
}
